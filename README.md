# Basis programmeren les 2

#### Voorbeelden over input, if/else statements, switch statements en for, while en do-while loops
https://gitlab.com/no299/basis-programmeren/les-2/-/blob/main/src/nl/novi/basisprogrammeren/InputAndIfAndWhile.java

#### Voorbeelden over arrays
https://gitlab.com/no299/basis-programmeren/les-2/-/blob/main/src/nl/novi/basisprogrammeren/Arrays.java

#### Voorbeelden over lists
https://gitlab.com/no299/basis-programmeren/les-2/-/blob/main/src/nl/novi/basisprogrammeren/Lists.java

#### Voorbeelden over methodes
https://gitlab.com/no299/basis-programmeren/les-2/-/blob/main/src/nl/novi/basisprogrammeren/Methodes.java

https://gitlab.com/no299/basis-programmeren/les-2/-/blob/main/src/nl/novi/basisprogrammeren/IntegerUtils.java
