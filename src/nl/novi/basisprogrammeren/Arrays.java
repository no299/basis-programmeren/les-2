package nl.novi.basisprogrammeren;

public class Arrays {

    public static void main(String[] args) {
        // Nieuwe array met items los
        String[] list = new String[3];
        list[0] = "1";
        list[1] = "2";
        list[2] = "3";

        for (int index = 0; index < list.length; index++) {
            System.out.println(list[index]);
        }

        // Nieuwe array met items in 1 keer
        int[] integerList = { 20, 30 };

        for (int item: integerList) {
            System.out.println(item);
        }
    }
}
