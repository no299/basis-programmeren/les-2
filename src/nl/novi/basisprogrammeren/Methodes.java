package nl.novi.basisprogrammeren;

import java.util.Arrays;

public class Methodes {

    public static void main(String[] args) {
        int[] lijst = {4, 6, 2, 7, 9, 10};

        // Methoede in een andere class (./IntegerUtils.java)
        int maximum = IntegerUtils.max(lijst);
        System.out.println(maximum);

        // Methoede in dezelfde class
        int minimum = min(lijst);
        System.out.println(minimum);
    }

    // Methoede in dezelfde class
    public static int min(int[] list) {
        Arrays.sort(list);
        return list[0];
    }
}
