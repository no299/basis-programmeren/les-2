package nl.novi.basisprogrammeren;

import java.util.ArrayList;
import java.util.List;

public class Lists {

    public static void main(String[] args) {
        // Nieuwe lijst met items los
        ArrayList<String> list = new ArrayList<>();
        list.add("test 1");
        list.add("test 2");
        list.add("test 3");
        list.add("test 4");

        list.remove(2);

        for(String item: list){
            System.out.println(item);
        }

        // Nieuwe lijst met items in 1 keer
        List<String> otherList = List.of("list item 1", "list item 2", "list item 3");

        for(String item: otherList){
            System.out.println(item);
        }
    }
}
