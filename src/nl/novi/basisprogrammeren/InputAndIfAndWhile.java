package nl.novi.basisprogrammeren;

import java.util.Scanner;

public class InputAndIfAndWhile {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // If/else statement
        System.out.println("Input number for for if/else");
        int ifInput = scanner.nextInt();
        if(ifInput < 3){
            System.out.println("input minder dan 3");
        } else if(ifInput < 6){
            System.out.println("input minder dan 6");
        } else {
            System.out.println("input meer dan 6");
        }

        // switch statement
        System.out.println("Input number for for switch");
        int switchInput = scanner.nextInt();
        switch (switchInput){
            case 1:
                System.out.println("Input was 1");
                break;
            case 2:
                System.out.println("Input was 2");
                break;
            case 3:
                System.out.println("Input was 3");
                break;
            case 4:
                System.out.println("Input was 4");
                break;
            default:
                System.out.println("Input was hoger dan 4");
        }

        // For loop
        System.out.println("Input number for for iterations");
        int forInput = scanner.nextInt();
        for(int i = 0; i < forInput; i++){
            System.out.println("index: " + i);
        }

        // While loop
        System.out.println("Input number for while iterations");
        int whileInput = scanner.nextInt();
        while(whileInput > 0){
            System.out.println("index: " + whileInput);
            whileInput--;
        }

        // Do-while loop
        System.out.println("Input number for do-while iterations");
        int doWhileInput = scanner.nextInt();
        do{
            System.out.println("index: " + doWhileInput);
            doWhileInput--;
        }while(doWhileInput > 0);
    }
}
