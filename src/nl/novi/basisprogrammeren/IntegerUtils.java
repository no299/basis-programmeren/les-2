package nl.novi.basisprogrammeren;

import java.util.Arrays;

public class IntegerUtils {

    // Methoede in een andere class (wordt gebruikt in ./Methodes.java)
    public static int max(int[] list) {
        Arrays.sort(list);
        return list[list.length - 1];
    }
}
